import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import json.Commits;
import org.apache.commons.io.FileUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MirthBridgeGui {
    private JSlider slider1;
    public JPanel panel1;
    private JComboBox<MirthServerInfo> comboBox1;
    private JButton connectButton;
    private JLabel ipAddress;
    MirthServerInfo selectedServer;
    private MirthServerInfo tempInfo;
    private List<MirthServerInfo> servers;
    private String tempVal;
    private JFrame frame;

    public MirthBridgeGui() {
        this.servers = new ArrayList();


        setFrame();
        fillCombo();

        this.comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedServer = ((MirthServerInfo) comboBox1.getSelectedItem());
                ipAddress.setText(selectedServer.getIpAddress());
                panel1.grabFocus();
            }
        });
        this.connectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (MirthBridgeGui.this.selectedServer != null) {
                    try {
                        FileUtils.copyURLToFile(new URL(selectedServer.getIpAddress() + "webstart.jnlp"),
                                new File("/tmp/" + selectedServer.getName() + "-webstart.jnlp"), 5000, 5000);
                    } catch (SocketTimeoutException ex) {
                        JOptionPane.showMessageDialog(MirthBridgeGui.this.frame, "Failed to retrieve Mirth application!");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    try {
                        Desktop.getDesktop().open(new File("/tmp/" + selectedServer.getName() + "-webstart.jnlp"));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        comboBox1.setSelectedIndex(0);
    }

    private void fillCombo() {
        parseDocument();
        for (MirthServerInfo info : servers) {
            comboBox1.addItem(info);
        }
    }

    private void parseDocument() {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            SAXParser sp = spf.newSAXParser();


            try {

                FileUtils.copyURLToFile(new URL("https://bitbucket.org/api/2.0/repositories/sandlotsolutions/mirth-servers/commits"),
                        new File("/tmp/" + "commits.json"), 5000, 5000)
                ;


                Reader reader = new InputStreamReader(new FileInputStream("/tmp/commits.json"), "UTF-8");

                Gson gson = new GsonBuilder().create();
                Commits p = gson.fromJson(reader, Commits.class);


                FileUtils.copyURLToFile(new URL("https://bitbucket.org/sandlotsolutions/mirth-servers/raw/" + p.getCommits().get(0).getHash() + "/servers.xml"),
                        new File("/tmp/" + "servers.xml"), 5000, 5000)
                ;

                sp.parse("/tmp/servers.xml", new MirthBridgeGui.XmlHandler());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(frame, "Could not load server listing from Online. Loading backup.");
                sp.parse("jar:" + getClass().getClassLoader().getResource("servers.xml").getFile(), new MirthBridgeGui.XmlHandler());
            }
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public void setFrame() {
        frame = new JFrame("Sandlot Mirth Bridge");
        frame.setContentPane(this.panel1);
        frame.setDefaultCloseOperation(3);
        frame.pack();
        frame.setVisible(true);

        URL iconURL = getClass().getResource("sandlotlogo.png");
        ImageIcon icon = new ImageIcon(iconURL);
        frame.setIconImage(icon.getImage());
    }

    public JFrame getFrame() {
        return frame;
    }

    public class XmlHandler
            extends DefaultHandler {
        public XmlHandler() {
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes)
                throws SAXException {
            tempVal = "";
            if (qName.equalsIgnoreCase("Mirth")) {
                tempInfo = new MirthServerInfo();
            }
        }

        public void characters(char[] ch, int start, int length)
                throws SAXException {
            tempVal = new String(ch, start, length);
        }

        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            if (qName.equalsIgnoreCase("Mirth")) {
                servers.add(tempInfo);
            } else if (qName.equalsIgnoreCase("Name")) {
                tempInfo.setName(tempVal);
            } else if (qName.equalsIgnoreCase("IpAddress")) {
                tempInfo.setIpAddress(tempVal);
            }
        }
    }
}

