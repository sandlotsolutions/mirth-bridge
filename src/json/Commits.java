package json;

import java.util.List;

/**
 * Created by mvaldez on 4/15/2015.
 */
public class Commits {
    private List<Commit> values;

    public List<Commit> getCommits() {
        return values;
    }

    public void setCommits(List<Commit> commits) {
        this.values = commits;
    }
}
