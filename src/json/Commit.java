package json;

/**
 * Created by mvaldez on 4/15/2015.
 */
public class Commit {
    private String hash;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
