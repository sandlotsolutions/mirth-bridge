public class MirthServerInfo {
    private String IpAddress;
    private String Name;

    public MirthServerInfo(String ipAddress, String name) {
        this.IpAddress = ipAddress;
        this.Name = name;
    }

    public MirthServerInfo() {
    }

    public String getIpAddress() {
        return this.IpAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.IpAddress = ipAddress;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String toString() {
        return this.Name;
    }
}
